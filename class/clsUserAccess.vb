﻿Public Class clsAksesUser
    Private m_UserName As String
    Public Property UserName As String
        Get
            Return m_UserName
        End Get
        Set(value As String)
            m_UserName = value
            End Set
    End Property

    Private m_ErrMsg As String
    Public Property ErrMsg As String
        Get
            Return m_ErrMsg
        End Get
        Set(value As String)
            m_ErrMsg = value
        End Set
    End Property

    Private Function DoLogin(ByVal xUser As String, ByVal xPass As String) As Boolean
        Dim xRetVal As Boolean : xRetVal = True

        If Trim(xUser) = "" Then
            xRetVal = False
            m_ErrMsg = "Invalid User Name"
        End If

        If xRetVal Then
            If Trim(xPass) = "" Then
                xRetVal = False
                m_ErrMsg = "Invalid password"
            End If
        End If

        Dim xSQL As String
        xSQL = " SELECT    username " _
             & " FROM      pengguna " _
             & " WHERE     username = " & QuoteStr(xUser) _
             & "       AND password = MD5(" & QuoteStr(xPass) & ") " _
             & "       AND hakakses = 1 "
        Dim xoDT As DataTable = GetRows(xSQL, gConn)
        xRetVal = xoDT.Rows.Count > 0
        If Not xRetVal Then
            m_ErrMsg = "Kombinasi username/password salah"
        End If

        xoDT.Dispose()

        Return xRetVal
    End Function

    Function CanLogin(ByVal xPass As String) As Boolean
        Return DoLogin(m_UserName, xPass)
    End Function
End Class
