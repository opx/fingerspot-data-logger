﻿Public Class clsLogMesinItem
    'xEnroll, xVerify, xDwInOut, xYear, xMonth, xDay, xHour, xMinute, xSec, xWorkCode
    Public dwEnrollNumber As Integer
    Public dwVerifyMode As Integer
    Public dwInOutMode As Integer
    Public dwYear As Integer
    Public dwMonth As Integer
    Public dwDay As Integer
    Public dwHour As Integer
    Public dwMinute As Integer
    Public dwSecond As Integer
    Public dwWorkCode As Integer
    Public dwDate As Date
    Public dwTime As DateTime
    Public TglLog As DateTime
End Class

Public Class clsLogMesinItems
    Inherits System.Collections.CollectionBase

    Overloads Property Count As Integer
        Get
            Return Me.List.Count
        End Get
        Set(value As Integer)

        End Set
    End Property

    Public Sub Add(ByVal xEnroll As Integer, ByVal xVerify As Integer, ByVal xInOut As Integer, ByVal xYear As Integer, _
                   ByVal xMonth As Integer, ByVal xDay As Integer, ByVal xHour As Integer, ByVal xMinute As Integer, _
                   ByVal xSecond As Integer, ByVal xWorkCode As Integer)
        Dim xoNewItem As New clsLogMesinItem
        With xoNewItem
            .dwEnrollNumber = xEnroll
            .dwVerifyMode = xVerify
            .dwInOutMode = xInOut
            .dwYear = xYear
            .dwMonth = xMonth
            .dwDay = xDay
            .dwHour = xHour
            .dwMinute = xMinute
            .dwSecond = xSecond
            .dwWorkCode = xWorkCode
            .dwDate = DateSerial(xYear, xMonth, xDay)
            .dwTime = TimeSerial(xHour, xMinute, xSecond)
            .TglLog = DateSerial(.dwYear, .dwMonth, .dwDay) + New TimeSpan(.dwHour, .dwMinute, .dwSecond)
        End With
        List.Add(xoNewItem)
        xoNewItem = Nothing
    End Sub

    Overloads Sub Clear()
        Me.List.Clear()
        Me.InnerList.Clear()
    End Sub

    Public Sub New()

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
