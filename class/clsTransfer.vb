﻿Public Enum eStatusMesinEvent As Integer
    estConnectMesin = 0
    estQueryLogMesin
End Enum

Public Class clsTransfer
    Private m_oMesin As clsMachine
    Private WithEvents m_oDevice As clsDevice
    Private m_IdMesin As Integer
    Private m_SQLGen As clsSQLGenerator

    Public WithEvents LogData As clsLogMesinItems
    '---untuk mulai transfer manual dimulai dari jam berapa s/d jam berapa
    '   value berupa date time, date ngambil dari jam pc (asumsi daemon diinstal di server)
    Public StartDate As DateTime
    Public EndDate As DateTime

    Public IsManualTransfer As Boolean

    Public ErrMsg As String

    Const CS_TABEL_LOG = "generallogdata"

    Private m_EventSection As Integer

    Private m_EventTgl As String
    Public Event GetStatus(ByRef xData As String, ByRef xSectionNum As Integer)

    Private m_EventSectionTransfer As Integer
    Public Event OnTransfer(ByRef xData As String, ByRef xSectionNum As Integer)

    Private Function GetInOutMode(ByVal xVal As Integer) As Integer
        '---hardcoding
        '   0 = check in
        '   1 = checkout
        '   2 = breakout
        '   3 = breakin
        '   4 = ot-in
        '   5 = ot-out

        Select Case xVal
            Case 0
                Return 1
            Case 1
                Return 257
            Case 2
                Return 1281
            Case 3
                Return 1025
            Case 4
                Return 513
            Case 5
                Return 769
        End Select
    End Function

    Function GetVerifyModeStr(ByVal xVal As Integer) As String
        Dim xRetVal As String : xRetVal = ""
        Select Case xVal
            Case 0
                xRetVal = "Check In"
            Case 1
                xRetVal = "Check Out"
            Case 2
                xRetVal = "Break Out"
            Case 3
                xRetVal = "Break In"
            Case 4
                xRetVal = "Overtime In"
            Case 5
                xRetVal = "Overtime Out"
        End Select
        Return xRetVal
    End Function

    Public Property IdMesin
        Get
            Return m_IdMesin
        End Get
        Set(value)
            m_IdMesin = value
        End Set
    End Property

    Private Function GetInitTime() As DateTime
        Return Now.Date + New TimeSpan(5, 0, 0)
    End Function

    Public Function GetEndTime() As DateTime
        Return Now.Date + New TimeSpan(23, 0, 0)
    End Function

    Public Function GetEndTimeConfig(ByVal xData As String) As DateTime
        Dim xTimeData As RecTimeSpan

        xTimeData = ExpandExecTime(xData)

        Return Now.Date + New TimeSpan(xTimeData.Jam, xTimeData.Menit, xTimeData.Detik)
    End Function

    Public Function GetData(Optional ByVal xIdMesin As Integer = -1) As Boolean
        Dim xRetVal As Boolean : xRetVal = True

        LogData.Clear()

        If xIdMesin <> -1 Then m_IdMesin = xIdMesin

        m_oMesin.GetMachines(m_IdMesin)
        Dim oItemMesin As New clsMachineItem
        For Each oItemMesin In m_oMesin.Machines
            m_oDevice.IpAddress = oItemMesin.IpAddress
            RaiseEvent GetStatus("Hubungkan ke mesin @" & oItemMesin.IpAddress, eStatusMesinEvent.estConnectMesin)
            'If m_oDevice.ConnectToDevice(oItemMesin.Port, True, m_IdMesin) Then
            If m_oDevice.ConnectToDevice(oItemMesin.Port, True) Then
                RaiseEvent GetStatus("Sukses", eStatusMesinEvent.estConnectMesin)
                If Not IsManualTransfer Then
                    LogData = m_oDevice.QueryLogMesinByDate(GetInitTime, GetEndTime)
                Else
                    LogData = m_oDevice.QueryLogMesinByDate(StartDate, EndDate)
                End If
                RaiseEvent GetStatus("Ditemukan " & LogData.Count & " data log", eStatusMesinEvent.estQueryLogMesin)
            Else
                RaiseEvent GetStatus("Failed !" & oItemMesin.IpAddress, eStatusMesinEvent.estConnectMesin)
                xRetVal = False
            End If
        Next

        Return xRetVal
    End Function

    Public Function Transfer() As Boolean

        Dim xRetVal As Boolean : xRetVal = True
        Dim xoSQLGen As New clsSQLGenerator
        Dim xoItem As New clsLogMesinItem
        Dim xSQL As String

        With xoSQLGen
            .TableUtama = CS_TABEL_LOG
        End With
        For Each xoItem In LogData
            RaiseEvent OnTransfer("Transferring data [" & StrDateTime(xoItem.TglLog) & "]" & _
                                  " Enroll num: " & xoItem.dwEnrollNumber & _
                                  " - " & GetVerifyModeStr(xoItem.dwVerifyMode), m_EventSectionTransfer)
            If Not CekIfExist(m_IdMesin, xoItem) Then
                Try
                    xoSQLGen.Clear()
                    With xoSQLGen
                        .AddInt("dwMachineNumber", m_IdMesin)
                        .AddInt("dwEnrollNumber", xoItem.dwEnrollNumber)
                        .AddInt("dwVerifyMode", xoItem.dwVerifyMode)
                        .AddInt("dwInOutMode", GetInOutMode(xoItem.dwInOutMode))
                        .AddInt("dwYear", xoItem.dwYear)
                        .AddInt("dwMonth", xoItem.dwMonth)
                        .AddInt("dwDay", xoItem.dwDay)
                        .AddInt("dwHour", xoItem.dwHour)
                        .AddInt("dwMinute", xoItem.dwMinute)
                        .AddDate("dwDate", xoItem.dwDate)
                        .AddTime("dwTime", xoItem.dwTime)
                    End With
                    xSQL = xoSQLGen.GenerateSQL(eSQLGenMethod.esgInsert)
                    xRetVal = ExecQuery(xSQL, gConn)
                    RaiseEvent OnTransfer("Sukses", m_EventSectionTransfer)
                Catch ex As Exception
                    xRetVal = False
                    ErrMsg = Err.Description
                    RaiseEvent OnTransfer("Gagal: " & ErrMsg, m_EventSectionTransfer)
                End Try
            Else
                RaiseEvent OnTransfer("Dilewati: data sudah ditransfer sebelumnya", m_EventSectionTransfer)
            End If
            m_EventSectionTransfer = m_EventSectionTransfer + 1
        Next

        RaiseEvent GetStatus("", 0)

        Return xRetVal
    End Function

    Public Function CekIfExist(ByVal xMachineID As Integer, ByVal xData As clsLogMesinItem) As Boolean
        Dim xRetVal As Boolean

        Dim xSQL As String
        xSQL = " SELECT     dwMachineNumber " _
             & " FROM    " & CS_TABEL_LOG _
             & " WHERE      dwMachineNumber = " & xMachineID _
             & "        AND dwEnrollNumber = " & xData.dwEnrollNumber _
             & "        AND dwVerifyMode = " & xData.dwVerifyMode _
             & "        AND dwInOutMode = " & GetInOutMode(xData.dwInOutMode) _
             & "        AND dwDate = " & QuoteStr(Format(xData.dwDate, "yyyy-MM-dd")) _
             & "        AND dwTime = " & QuoteStr(Format(xData.dwTime, "HH:mm:ss"))

        Dim xoDT As DataTable = GetRows(xSQL, gConn)
        If xoDT.Rows.Count > 0 Then
            xRetVal = xoDT.Rows.Count > 0
        End If

        Return xRetVal
    End Function

    Public Sub New()
        m_EventSectionTransfer = 0
        m_oMesin = New clsMachine : m_oMesin.IsQueryMachine = False
        m_oDevice = New clsDevice
        LogData = New clsLogMesinItems
        m_SQLGen = New clsSQLGenerator
    End Sub

    Protected Overrides Sub Finalize()
        m_oMesin = Nothing
        m_oDevice = Nothing
        LogData = Nothing
        m_SQLGen = Nothing
        MyBase.Finalize()
    End Sub

    Private Sub m_oDevice_QueryLogData(ByRef xTgl As DateTime) Handles m_oDevice.QueryLogData
        If m_EventTgl <> StrDate(xTgl) Then
            RaiseEvent GetStatus("Membaca data mesin tanggal: " & StrDate(xTgl), eStatusMesinEvent.estQueryLogMesin)
            m_EventTgl = StrDate(xTgl)
        End If
    End Sub
End Class
