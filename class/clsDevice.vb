﻿Option Explicit On

Imports zkemkeeper

'---akomodir info device
'   per class per mesin
Public Enum enumDownloadMode As Integer
    dmTransaksi = 0
    dmUser
End Enum

Public Class clsDevice

    Private m_DevSdk As CZKEM

    Private m_DeviceNumber As Integer

    Public IpAddress As String
    Public MacAddress As String
    Public Password As String
    Public Port As Integer
    Public FirmwareVersion As String
    Public SerialNumber As String
    Public DeviceID As Integer
    'Public isConnected As Boolean
    Public BaudRate As String

    Public ErrMsg As String

    '---konek ke alat
    Public Event TryingConnect()
    Public Event EndTry()
    Public Event GetDeviceInfo()
    Public Event EndGetDeviceInfo()

    '---query log di alat
    Public Event QueryLogData(ByRef xTgl As DateTime)

    Private m_IsConnected As Boolean
    Public Property IsConnected As Boolean
        Get
            Return m_IsConnected
        End Get
        Set(value As Boolean)

        End Set
    End Property

    Public Function ConnectToDevice(ByVal xPort As Integer, Optional ByVal xOnlyConnect As Boolean = False, _
                                    Optional ByVal xNoMesin As Integer = -1) As Boolean
        Dim xRetVal As Boolean : xRetVal = True

        MacAddress = ""
        Port = 0
        FirmwareVersion = ""
        SerialNumber = ""
        m_IsConnected = False
        DeviceID = -1

        If xNoMesin = -1 Then
            m_DeviceNumber = 1
        Else
            m_DeviceNumber = xNoMesin
            m_DeviceNumber = 1
        End If


        If Trim(IpAddress) = "" Then
            xRetVal = False
            ErrMsg = "IP Address invalid"
        End If

        If xRetVal Then
            If xPort = 0 Then
                xRetVal = False
                ErrMsg = "Port invalid"
            End If
        End If

        If xRetVal Then
            RaiseEvent TryingConnect()
            xRetVal = m_DevSdk.Connect_Net(IpAddress, xPort)
            RaiseEvent EndTry()
        End If

        If xRetVal Then
            m_IsConnected = True
            '---param xOnlyConnect set true jika hanya konek ke mesin saja
            '   jika sebaliknya maka device info akan ikut dibaca
            If Not xOnlyConnect Then
                RaiseEvent GetDeviceInfo()
                m_DevSdk.GetDeviceMAC(m_DeviceNumber, MacAddress)
                m_DevSdk.GetSerialNumber(m_DeviceNumber, SerialNumber)
                m_DevSdk.GetFirmwareVersion(m_DeviceNumber, FirmwareVersion)

                Dim xBaudRateVal As Integer
                m_DevSdk.GetDeviceInfo(m_DeviceNumber, 9, xBaudRateVal)
                BaudRate = Format((xBaudRateVal + 1) * 1200, "#,### bps")
                RaiseEvent EndGetDeviceInfo()
            End If
        End If

            Return xRetVal
    End Function

    Public Function QueryLogMesinByDate(ByVal xTglAwal As DateTime, ByVal xTglAkhir As DateTime) As clsLogMesinItems
        Dim xRetVal As New clsLogMesinItems
        Dim xoItemLog As New clsLogMesinItem
        Dim xTmpTgl As DateTime

        m_DevSdk.ReadAllGLogData(m_DeviceNumber)

        With xoItemLog

            While m_DevSdk.SSR_GetGeneralLogData(m_DeviceNumber, .dwEnrollNumber, .dwVerifyMode, .dwInOutMode, .dwYear, .dwMonth, _
                                                 .dwDay, .dwHour, .dwMinute, .dwSecond, .dwWorkCode)
                xTmpTgl = DateSerial(.dwYear, .dwMonth, .dwDay) + New TimeSpan(.dwHour, .dwMinute, .dwSecond)
                If xTmpTgl > xTglAwal And xTmpTgl < xTglAkhir Then
                    RaiseEvent QueryLogData(xTmpTgl)
                    xRetVal.Add(.dwEnrollNumber, .dwVerifyMode, .dwInOutMode, .dwYear, .dwMonth, .dwDay, .dwHour, _
                                .dwMinute, .dwSecond, .dwWorkCode)
                End If
            End While
        End With
        Return xRetVal
        xRetVal = Nothing
    End Function

    Public Function QueryLogMesinAll() As clsLogMesinItems
        Dim xRetVal As New clsLogMesinItems
        Dim xoItemLog As New clsLogMesinItem

        m_DevSdk.ReadAllGLogData(m_DeviceNumber)

        With xoItemLog
            While m_DevSdk.SSR_GetGeneralLogData(m_DeviceNumber, .dwEnrollNumber, .dwVerifyMode, .dwInOutMode, .dwYear, .dwMonth, _
                                                 .dwDay, .dwHour, .dwMinute, .dwSecond, .dwWorkCode)
                RaiseEvent QueryLogData(DateSerial(.dwYear, .dwMonth, .dwDay) + " " + TimeSerial(.dwHour, .dwMinute, .dwSecond))
                xRetVal.Add(.dwEnrollNumber, .dwVerifyMode, .dwInOutMode, .dwYear, .dwMonth, .dwDay, .dwHour, _
                            .dwMinute, .dwSecond, .dwWorkCode)
            End While
        End With
        Return xRetVal
        xRetVal = Nothing
    End Function



    Public Sub New()
        m_DevSdk = New CZKEM
    End Sub

    Protected Overrides Sub Finalize()
        m_DevSdk = Nothing
        MyBase.Finalize()
    End Sub
End Class
