﻿Option Explicit On

Imports BiometricExport.modAppSetting

Public Class clsAppSetting

    Public DbaMachineLogConfig As clsServerConfig
    Public ExecTime As String ' format hh:mm locale ID
    Public ErrMsg As String

    Private Const CS_KEYNAME_CONN = "Connection"
    Private Const CS_KEYNAME_SERVICE = "Service"

    Public Sub New()
        DbaMachineLogConfig = New clsServerConfig
        
        GetServerConfig()


    End Sub

    Private Sub GetServerConfig()
        Dim xStrPrefix As String

        xStrPrefix = "FingerspotHost"
        With DbaMachineLogConfig
            .ServerName = meReadSettingStr(CS_KEYNAME_CONN, xStrPrefix + "DbServer")
            .DbUser = meReadSettingStr(CS_KEYNAME_CONN, xStrPrefix + "DbUser")
            .DbPass = meReadSettingStr(CS_KEYNAME_CONN, xStrPrefix + "DbPwd")
            .DbPort = meReadSettingStr(CS_KEYNAME_CONN, xStrPrefix + "DbPort", "3306")
            .DbName = meReadSettingStr(CS_KEYNAME_CONN, xStrPrefix + "DbName", "presensi")
            .ExecTime = meReadSettingStr(CS_KEYNAME_SERVICE, "ExecTime", "23:00")
            If Not IsValidTime(.ExecTime) Then
                .ExecTime = "23:00"
            End If
        End With

    End Sub

    Private Function SaveServerConfig() As Boolean
        Dim xGo As Boolean : xGo = True

        If Trim(DbaMachineLogConfig.ServerName) = "" Then
            xGo = False
            ErrMsg = "Server name  invalid"
        End If

        If xGo Then
            If Trim(DbaMachineLogConfig.DbName) = "" Then
                xGo = False
                ErrMsg = "Database name invalid"
            End If
        End If

        If xGo Then
            If Trim(DbaMachineLogConfig.DbUser) = "" Then
                xGo = False
                ErrMsg = "Username invalid"
            End If
        End If

        If xGo Then
            Dim xPrefix As String

            xPrefix = "FingerspotHost"
            With DbaMachineLogConfig
                meSaveSettingStr(CS_KEYNAME_CONN, xPrefix + "DbServer", .ServerName)
                meSaveSettingStr(CS_KEYNAME_CONN, xPrefix + "DbUser", .DbUser)
                meSaveSettingStr(CS_KEYNAME_CONN, xPrefix + "DbPwd", .DbPass)
                meSaveSettingStr(CS_KEYNAME_CONN, xPrefix + "DbPort", .DbPort)
                meSaveSettingStr(CS_KEYNAME_CONN, xPrefix + "DbName", .DbName)
                meSaveSettingStr(CS_KEYNAME_SERVICE, "ExecTime", .ExecTime)
            End With
        End If

        Return xGo
    End Function

    Public Function Save() As Boolean
        Dim xGo As Boolean : xGo = True

        'meSaveSettingStr(CS_KEYNAME_SERVICE, "ExecTime", ExecTime)

        xGo = SaveServerConfig()
        
        Return xGo
    End Function

    Protected Overrides Sub Finalize()
        DbaMachineLogConfig = Nothing
        MyBase.Finalize()
    End Sub
End Class