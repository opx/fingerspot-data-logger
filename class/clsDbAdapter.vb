﻿Imports MySql.Data.MySqlClient

Public Class clsDbAdapter
    Public ErrMsg As String

    Private m_MySQLConn As MySqlConnection
    Private m_SvrConfig As clsServerConfig

    Private Function GenConnectionString(ByVal xParam As clsServerConfig) As String
        Dim xRetVal As String

        xRetVal = "server=" + xParam.ServerName + ";" _
                + "user id=" + xParam.DbUser + ";" _
                + "password=" + xParam.DbPass + ";" _
                + "port=" + xParam.DbPort + ";" _
                + "database=" + xParam.DbName

        Return xRetVal
    End Function

    Public Sub AssignConnection(ByVal xConn As MySqlConnection)
        If IsNothing(xConn) Then Exit Sub

        '---harus test connection dulu


        xConn.Close()
        xConn.ConnectionString = GenConnectionString(m_SvrConfig)
        xConn.Open()

    End Sub

    Public Function ConnectToServer(ByVal xConn As MySqlConnection, ByVal xParam As clsServerConfig) As Boolean
        Dim xGo As Boolean

        xGo = TestConnection(xParam)

        If xGo Then
            AssignConnection(xConn)
        End If

        Return xGo
    End Function

    Public Function TestConnection(ByVal xServerConfig As clsServerConfig, _
                                   Optional ByRef xConnStr As String = "") As Boolean
        Dim xRetVal As Boolean

        m_MySQLConn.ConnectionString = GenConnectionString(xServerConfig)
        Try
            m_MySQLConn.Open()
            xRetVal = True
        Catch ex As Exception
            ErrMsg = ex.Message
            xRetVal = False
        End Try

        If xRetVal Then
            xConnStr = m_MySQLConn.ConnectionString

            With m_SvrConfig
                .ServerName = xServerConfig.ServerName
                .DbUser = xServerConfig.DbUser
                .DbPass = xServerConfig.DbPass
                .DbPort = xServerConfig.DbPort
                .DbName = xServerConfig.DbName
            End With
        End If

        m_MySQLConn.Close()

        Return xRetVal
    End Function

    Public Sub New()
        m_MySQLConn = New MySqlConnection
        m_SvrConfig = New clsServerConfig
    End Sub

    Protected Overrides Sub Finalize()
        m_MySQLConn.Dispose()
        m_SvrConfig = Nothing
        MyBase.Finalize()
    End Sub
End Class
