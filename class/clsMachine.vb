﻿Option Explicit On

Imports MySql.Data.MySqlClient

Public Class clsMachineItem
    Public MachineNumber As Integer
    Public IpAddress As String
    Public MacAddress As String
    Public Password As String
    Public Port As Integer
    Public FirmwareVersion As String
    Public SerialNumber As String
    Public IsEnabled As Boolean
    Public Keterangan As String
    Public DeviceData As clsDevice

    Public Sub New()
        DeviceData = New clsDevice
    End Sub

    Protected Overrides Sub Finalize()
        DeviceData = Nothing
        MyBase.Finalize()
    End Sub
End Class

Public Class clsMachineItems
    Inherits System.Collections.CollectionBase

    Public Sub Add(ByVal xMachineNum As Integer, ByVal xIp As String, ByVal xMac As String, ByVal xPassword As String, _
                   ByVal xPort As Integer, ByVal xFirmware As String, ByVal xSerial As String, _
                   ByVal xIsEnabled As Boolean, ByVal xKeterangan As String, Optional ByVal xQueryMachine As Boolean = False)
        Dim xoNewItem As New clsMachineItem
        With xoNewItem
            .MachineNumber = xMachineNum
            .IpAddress = xIp
            .MacAddress = xMac
            .Password = xPassword
            .Port = xPort
            .FirmwareVersion = xFirmware
            .SerialNumber = xSerial
            .IsEnabled = xIsEnabled
            .Keterangan = xKeterangan

            If xQueryMachine Then
                .DeviceData.IpAddress = xIp
                .DeviceData.ConnectToDevice(xPort)
            End If
        End With
        List.Add(xoNewItem)
        xoNewItem = Nothing
    End Sub

    Public Property ItemById(ByVal xMachineNum As Integer) As clsMachineItem
        Get
            Dim xoRetVal As clsMachineItem
            Dim xoItem As clsMachineItem

            For Each xoItem In List
                If xMachineNum = xoItem.MachineNumber Then
                    xoRetVal = xoItem
                    Exit For
                End If
            Next

            If IsNothing(xoRetVal) Then
                Return New clsMachineItem
            Else
                Return xoRetVal
            End If

            xoRetVal = Nothing
        End Get
        Set(value As clsMachineItem)

        End Set
    End Property

    Public Property GetItem(ByVal xIndex As Integer) As clsMachineItem
        Get
            Return Me.List.Item(xIndex)
        End Get
        Set(value As clsMachineItem)

        End Set
    End Property
    Overloads Sub Clear()
        Me.List.Clear()
        Me.InnerList.Clear()
    End Sub

    Public Sub New()

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class

Public Class clsMachine
    Public Machines As clsMachineItems
    Public IsQueryMachine As Boolean
    Public IsShowAktifOnly As Boolean

    Public Sub GetMachines(Optional ByVal xMachineID As Integer = 0)

        Machines.Clear()

        Dim xSQL As String
        Dim i As Integer
        xSQL = " SELECT     dwMachineNumber, dwIPAddress, dwPort, dwPassword, dwDeviceMAC, dwFirmwareVersion, " _
             & "            dwSerialNumber, dwTitle, dwEnable " _
             & " FROM       MACHINE "
        If xMachineID > 0 Then
            If InStr(xSQL, "WHERE") Then
                xSQL = xSQL & " AND "
            Else
                xSQL = xSQL & " WHERE"
            End If
            xSQL = xSQL _
                 & " dwMachineNumber = " & xMachineID
        Else
            If IsShowAktifOnly Then
                If InStr(xSQL, "WHERE") Then
                    xSQL = xSQL & " AND "
                Else
                    xSQL = xSQL & " WHERE "
                End If
                xSQL = xSQL _
                     & " dwEnable <> -1 "
            End If
        End If
        xSQL = xSQL & " ORDER BY dwMachineNumber ASC "

        Dim xoDT As DataTable = GetRows(xSQL, gConn)
        If xoDT.Rows.Count > 0 Then
            For i = 0 To xoDT.Rows.Count - 1
                Machines.Add(xoDT.Rows(i)("dwMachineNumber").ToString, _
                             xoDT.Rows(i)("dwIpAddress").ToString, _
                             xoDT.Rows(i)("dwDeviceMac").ToString, _
                             xoDT.Rows(i)("dwPassword").ToString,
                             xoDT.Rows(i)("dwPort").ToString, _
                             xoDT.Rows(i)("dwFirmwareVersion").ToString, _
                             xoDT.Rows(i)("dwSerialNumber").ToString, _
                             IIf(xoDT.Rows(i)("dwEnable") = -1, False, True), _
                             xoDT.Rows(i)("dwTitle").ToString, IsQueryMachine)
            Next i
        End If
        xoDT.Dispose()
    End Sub

    Public Sub New()
        Machines = New clsMachineItems
    End Sub

    Protected Overrides Sub Finalize()
        Machines = Nothing
        MyBase.Finalize()
    End Sub
End Class
