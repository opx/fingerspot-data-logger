﻿'---SQL GENERATOR
'   class untuk generate query sql insert dan update
'   mockheroicx@yahoo.com
'   Copyright(c) 2015 opx

Option Explicit On

Public Enum eSQLGenMethod As Integer
    esgInsert = 0
    esgUpdate
End Enum

Public Class clsSQLFieldItem
    Public FieldName As String
    Public DataValue As String
End Class

Public Class clsSQLFieldItems
    Inherits System.Collections.CollectionBase

    Public Sub Add(ByVal xFieldName As String, ByVal xValue As String)
        Dim xoItem As New clsSQLFieldItem
        With xoItem
            .FieldName = xFieldName
            .DataValue = xValue
        End With
        List.Add(xoItem)
        xoItem = Nothing
    End Sub

    Overloads Sub Clear()
        Me.List.Clear()
        Me.InnerList.Clear()
    End Sub
End Class

Public Class clsSQLGenerator
    Private m_Fields As clsSQLFieldItems

    Private m_Table As String
    Private m_KeyField As String
    Private m_KeyValue As String

    Public Property TableUtama As String
        Get
            Return m_Table
        End Get
        Set(value As String)
            m_Table = value
        End Set
    End Property

    Public Property KeyField As String
        Get
            Return m_KeyField
        End Get
        Set(value As String)
            m_KeyField = value
        End Set
    End Property

    Public Property KeyValue As String
        Get
            Return m_KeyValue
        End Get
        Set(value As String)
            m_KeyValue = value
        End Set
    End Property

    Public Sub AddString(ByVal xFieldName As String, ByVal xValue As String)
        m_Fields.Add(xFieldName, QuoteStr(xValue))
    End Sub

    Public Sub AddInt(ByVal xFieldName As String, ByVal xValue As Integer)
        m_Fields.Add(xFieldName, xValue)
    End Sub

    Public Sub AddDate(ByVal xFieldName As String, ByVal xValue As Date)
        '---default date format di MySQL = yyyy-MM-dd
        AddString(xFieldName, Format(xValue, "yyyy-MM-dd"))
    End Sub

    Public Sub AddTime(ByVal xFieldName As String, ByVal xTime As DateTime)
        AddString(xFieldName, Format(xTime, "HH:mm:ss"))
    End Sub

    Public Sub AddDateTime(ByVal xFieldName As String, ByVal xValue As DateTime)
        AddString(xFieldName, Format(xValue, "yyyy-MM-dd HH:mm:ss"))
    End Sub

    Function GenerateSQL(ByVal xGenMethod As eSQLGenMethod) As String
        Dim xRetVal As String : xRetVal = ""
        Dim xFieldDef As String : xFieldDef = ""
        Dim xValueDef As String : xValueDef = ""
        Dim xoItem As clsSQLFieldItem

        Select Case xGenMethod
            Case eSQLGenMethod.esgInsert
                For Each xoItem In m_Fields
                    xFieldDef = xFieldDef & xoItem.FieldName & ","
                    xValueDef = xValueDef & xoItem.DataValue & ","
                Next
                xFieldDef = Left(xFieldDef, Len(xFieldDef) - 1)
                xValueDef = Left(xValueDef, Len(xValueDef) - 1)
                xRetVal = " INSERT INTO " & m_Table _
                        & " ( " & xFieldDef & " ) " & " VALUES " _
                        & " ( " & xValueDef & " ) "
            Case eSQLGenMethod.esgUpdate
                For Each xoItem In m_Fields
                    xFieldDef = xFieldDef & xoItem.FieldName & "=" & xoItem.DataValue & ","
                Next
                xFieldDef = Left(xFieldDef, Len(xFieldDef) - 1)
                xRetVal = " UPDATE " & m_Table & " SET " _
                        & xFieldDef
                If Trim(m_KeyField) <> "" Then
                    xRetVal = xRetVal _
                            & " WHERE " & m_KeyField & "=" & QuoteStr(m_KeyValue)
                End If
        End Select

        Return xRetVal
    End Function

    Public Sub Clear()
        m_Fields.Clear()
    End Sub

    Public Sub New()
        m_Fields = New clsSQLFieldItems
    End Sub

    Protected Overrides Sub Finalize()
        m_Fields = Nothing
        MyBase.Finalize()
    End Sub
End Class


