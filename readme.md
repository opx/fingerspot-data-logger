Tested Device:
FingerSpot + face recognition

To configure your own database & setup your custom database target properties, Please see the following files:
clsAppSetting.vb,
clsDbAdapter.vb,
clsTransfer.vb

Core Machine classes (Log Reader):
clsDevice.vb,
clsMachine.vb,
clsLogMesin.vb

Initial Database: MySQL

Requirements 3rd party:
ZKemKeeper, MySQLConnector(Depends your DB)