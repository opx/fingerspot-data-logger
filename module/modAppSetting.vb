﻿Option Explicit On

Imports Microsoft.Win32

Module modAppSetting

    Private Const REG_KEY As String = "HKEY_CURRENT_USER\Software\FingerspotDaemon\"

    Public Function meReadSettingBool(ByVal xPath As String, ByVal xKey As String) As Boolean
        With My.Computer.Registry.CurrentUser.OpenSubKey(REG_KEY + xPath)
            Return .GetValue(xKey, 0) = 1
        End With
    End Function

    Public Function meReadSettingStr(ByVal xPath As String, ByVal xKey As String, Optional ByVal xDefault As String = "") As String
        'Dim xTmp As New Object
        Return My.Computer.Registry.GetValue(REG_KEY + xPath, xKey, xDefault)
    End Function

    Public Sub meSaveSettingStr(ByVal xPath As String, ByVal xName As String, xValue As String)
        My.Computer.Registry.SetValue(REG_KEY + xPath, xName, xValue, RegistryValueKind.String)
    End Sub

    Public Sub meSaveSettingBool(ByVal xPath As String, ByVal xName As String, xValue As Boolean)
        With My.Computer.Registry.CurrentUser
            If IsNothing(.OpenSubKey(REG_KEY + xPath)) Then
                .CreateSubKey(REG_KEY + xPath)
            End If
            .SetValue(xName, IIf(xValue, 1, 0))
        End With
    End Sub

    Public Sub meSetAsStartup()
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run", "DaemonBiometric", Process.GetCurrentProcess.MainModule.FileName)
    End Sub
End Module
