﻿Module modGridView
    Public Enum GridViewColType As Integer
        gvcString
        gvcBool
        gvcNumber
        gvcDate
    End Enum

    Public Sub GridAddColumn(ByVal xObject As DataGridView, ByVal xTitle As String, ByVal xType As GridViewColType, _
                             ByVal xWidth As Integer, ByVal xIsVisible As Boolean)
        If IsNothing(xObject) Then Exit Sub

        Dim xCol As Object

        With xObject

            Select Case xType
                Case GridViewColType.gvcBool
                    xCol = New DataGridViewCheckBoxColumn
                    With DirectCast(xCol, DataGridViewCheckBoxColumn)
                        .HeaderText = xTitle
                        .Width = xWidth
                        .Visible = xIsVisible
                        .SortMode = DataGridViewColumnSortMode.NotSortable
                    End With
                Case GridViewColType.gvcString
                    xCol = New DataGridViewTextBoxColumn
                    With DirectCast(xCol, DataGridViewTextBoxColumn)
                        .HeaderText = xTitle
                        .Visible = xIsVisible
                        .Width = xWidth
                        .SortMode = DataGridViewColumnSortMode.NotSortable
                    End With
                Case GridViewColType.gvcDate
                    xCol = New DataGridViewTextBoxColumn
                    With DirectCast(xCol, DataGridViewTextBoxColumn)
                        .HeaderText = xTitle
                        .Visible = xIsVisible
                        .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        .Width = xWidth
                        .SortMode = DataGridViewColumnSortMode.NotSortable
                    End With
                Case GridViewColType.gvcNumber
                    xCol = New DataGridViewTextBoxColumn
                    With DirectCast(xCol, DataGridViewTextBoxColumn)
                        .HeaderText = xTitle
                        .Visible = xIsVisible
                        .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        .Width = xWidth
                        .SortMode = DataGridViewColumnSortMode.NotSortable
                    End With
            End Select
            .Columns.Add(xCol)
        End With

        xCol = Nothing
    End Sub

    Public Function GridGetColIndexByHeader(ByVal xObject As DataGridView, ByVal xTitle As String) As Integer
        On Error Resume Next

        Dim xRetVal As Integer : xRetVal = 0
        Dim i As Integer

        For i = 0 To xObject.ColumnCount - 1
            If UCase(xTitle) = UCase(xObject.Columns.Item(i).HeaderText) Then
                xRetVal = i
                Exit For
            End If
        Next i

        Return xRetVal
    End Function
End Module
