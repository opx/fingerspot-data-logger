﻿Module modSistem
    '   {datetime}

    Public Structure RecTimeSpan
        Public Jam As Byte
        Public Menit As Byte
        Public Detik As Byte
    End Structure

    Function StrDate(ByVal xTgl As Date) As String
        Return Format(xTgl, "dd-MM-yyyy")
    End Function

    Function StrTime(ByVal xTgl As DateTime) As String
        Return Format(xTgl, "HH:mm:ss")
    End Function

    Function StrDateTime(ByVal xTgl As DateTime) As String
        Return Format(xTgl, "dd-MM-yyyy HH:mm:ss")
    End Function

    Function StrToDate(ByVal xStrDate As String) As Date
        Dim xTgl As String
        Dim xBln As String
        Dim xThn As String

        xTgl = Left(xStrDate, 2)
        xBln = Mid(xStrDate, 4, 2)
        xThn = Right(xStrDate, 4)
        Return DateSerial(xThn, xBln, xTgl)
    End Function

    Function StrToDateTime(ByVal xStrDate As String, ByVal xStrTime As String) As DateTime
        Dim xDate As Date
        xDate = StrToDate(xStrDate)

        Dim xJam As String : xJam = Left(xStrTime, 2)
        Dim xMenit As String : xMenit = Mid(xStrTime, 4, 2)
        Dim xDetik As String : xDetik = Right(xStrTime, 2)

        Return xDate & " " & TimeSerial(xJam, xMenit, xDetik)
    End Function

    Function QuoteStr(ByVal xStr As String) As String
        Return """" & xStr & """"
    End Function

    Public Function NowMidNight() As DateTime
        Return StrToDateTime(StrDate(Now), "00:00:00")
    End Function

    Public Function IsValidTime(ByVal xData As String) As Boolean
        Dim xRetVal As Boolean : xRetVal = True

        Dim xTmp As String
        Dim xIs6Digit As Boolean

        xRetVal = (Len(xData) = 8) Or (Len(xData) = 5)

        xIs6Digit = Len(xData) = 8

        'cek sign #1
        If xRetVal Then
            xRetVal = Mid$(xData, 3, 1) = ":"
        End If

        'cek sign #2
        If xRetVal Then
            If xIs6Digit Then
                xRetVal = Mid$(xData, 6, 1) = ":"
            End If
        End If

        'cek value jam
        If xRetVal Then
            xTmp = Left$(xData, 2)
            xRetVal = IsNumeric(xTmp)

            If xRetVal Then
                xRetVal = xTmp >= 0 And xTmp <= 23
            End If
        End If

        'cek value menit
        If xRetVal Then
            xTmp = Mid(xData, 4, 2)
            xRetVal = IsNumeric(xTmp)

            If xRetVal Then
                xRetVal = xTmp >= 0 And xTmp <= 59
            End If
        End If

        'cek value detik
        If xRetVal Then
            If xIs6Digit Then
                xTmp = Right$(xData, 2)
                xRetVal = IsNumeric(xTmp)

                If xRetVal Then
                    xRetVal = xTmp >= 0 And xTmp <= 59
                End If
            End If
        End If

        Return xRetVal
    End Function

    Function ExpandExecTime(ByVal xData As String) As RecTimeSpan
        Dim xRetVal As RecTimeSpan

        With xRetVal
            xRetVal.Jam = 23
            xRetVal.Menit = 0
            xRetVal.Detik = 0
        End With

        If Trim(xData) = "" Then
            GoTo EndF
        End If

        Dim xIs6Digit As Boolean

        xIs6Digit = Len(xData) = 8

        xRetVal.Jam = Left(xData, 2)
        xRetVal.Menit = Mid(xData, 4, 2)
        If Not xIs6Digit Then
            xRetVal.Menit = 0
        End If
EndF:
        Return xRetVal
    End Function
End Module
