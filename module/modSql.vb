﻿Imports MySql.Data.MySqlClient

Module modSql
    Public Function GetRows(ByVal Query As String, ByVal Conn As MySqlConnection) As DataTable
        Dim xoCmd As New MySqlCommand
        Dim xoSAD As New MySqlDataAdapter
        Dim xoDS As New DataSet
        Dim xoDT As DataTable
        xoCmd.CommandText = Query
        xoCmd.Connection = Conn
        xoCmd.CommandTimeout = 180
        xoSAD.SelectCommand = xoCmd
        xoSAD.Fill(xoDS)
        xoSAD.Dispose()
        xoDT = xoDS.Tables(0)
        Return xoDT
        xoDT.Dispose()
        xoDS.Dispose()
        xoSAD.Dispose()
        xoCmd.Dispose()
    End Function

    Public Function ExecQuery(ByVal xQuery As String, ByVal xConn As MySqlConnection) As Boolean
        Dim x As Integer
        Dim xoCmd = New MySqlCommand(xQuery, xConn)
        'Dim xoCmd = New MySqlCommand
        'xoCmd.Connection.Open()
        xoCmd.CommandText = xQuery
        x = xoCmd.ExecuteNonQuery
        If x = 0 Then
            Return False
        Else
            Return True
        End If
        xoCmd.Connection.Close()
        xoCmd = Nothing
    End Function
End Module


