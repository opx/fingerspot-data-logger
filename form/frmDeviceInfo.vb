﻿Public Class frmDeviceInfo
    Public IdMesin As String

    Private Sub cmdOk_Click(sender As Object, e As EventArgs) Handles cmdOk.Click
        Close()
    End Sub

    Private Sub frmDeviceInfo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim xoMesin As New clsMachine : xoMesin.IsQueryMachine = True

        xoMesin.GetMachines(IdMesin)

        txtInfo.Text = ""
        Dim xoItemMesin As New clsMachineItem
        For Each xoItemMesin In xoMesin.Machines
            txtInfo.Text = "Device information" & vbCrLf _
                         & "==================" & vbCrLf & vbCrLf _
                         & "Mac              : " & xoItemMesin.DeviceData.MacAddress & vbCrLf _
                         & "Serial Number    : " & xoItemMesin.DeviceData.SerialNumber & vbCrLf _
                         & "Firmware Version : " & xoItemMesin.DeviceData.FirmwareVersion & vbCrLf
        Next

        xoMesin = Nothing
    End Sub
End Class