﻿Option Explicit On

Imports BiometricExport.clsAppSetting
Imports BiometricExport.clsDbAdapter

Public Class frmSetupConn

    Private m_Settings As clsAppSetting
    Private m_DbAdapter As clsDbAdapter

    Private Sub frmSetupConn_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '---read config
        m_Settings = New clsAppSetting
        With m_Settings.DbaMachineLogConfig
            txtServer1.Text = .ServerName
            txtUser1.Text = .DbUser
            txtPass1.Text = .DbPass
            txtPort1.Value = .DbPort
            txtDatabase.Text = .DbName

            txtJam.Text = .ExecTime
        End With

        m_DbAdapter = New clsDbAdapter
    End Sub

    Private Sub frmSetupConn_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        m_Settings = Nothing
        m_DbAdapter = Nothing
    End Sub

    Private Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        With m_Settings.DbaMachineLogConfig
            .ServerName = txtServer1.Text
            .DbUser = txtUser1.Text
            .DbPass = txtPass1.Text
            .DbPort = txtPort1.Value
            .DbName = txtDatabase.Text
            .ExecTime = txtJam.Text
        End With

        If Not m_Settings.Save Then
            MsgBox(m_Settings.ErrMsg, MsgBoxStyle.Exclamation, "Error")
        Else
            If m_DbAdapter.TestConnection(m_Settings.DbaMachineLogConfig) Then
                '---set global connection
                m_DbAdapter.AssignConnection(gConn)
                If gConn.State = ConnectionState.Closed Then
                    gConn.Open()
                End If
                MsgBox("Konfigurasi sudah disimpan " & vbCrLf & "Silahkan restart ulang aplikasi daemon", MsgBoxStyle.Information, "Error")
                Close()
            Else
                MsgBox(m_DbAdapter.ErrMsg, MsgBoxStyle.Exclamation, "Error")
            End If
        End If
    End Sub

    Private Sub cmdTestConn_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim xoAdapter As New clsDbAdapter
        Dim xServerCfg As New clsServerConfig

        With xServerCfg
            .ServerName = txtServer1.Text
            .DbUser = txtUser1.Text
            .DbPass = txtPass1.Text
            .DbPort = txtPort1.Value
            .DbName = txtDatabase.Text
        End With
            

        If Not xoAdapter.TestConnection(xServerCfg) Then
            MsgBox("Connection test result: failed !", MsgBoxStyle.Exclamation, "Failed")
        Else
            MsgBox("Connection test result: O.K", MsgBoxStyle.Information, "Sukses")
        End If
        xoAdapter = Nothing
    End Sub

    Private Sub Label5_Click(sender As Object, e As EventArgs)

    End Sub
End Class