﻿Public Class frmTransferManual

    Private m_MachineList As clsMachineItems

    Private m_TransferSectionNum As Integer
    Private m_QueryLogNum As Integer
    Private WithEvents m_Transferer As clsTransfer


    Private Sub frmTransferManual_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        CheckForIllegalCrossThreadCalls = False

        txtTgl1.Value = NowMidNight()
        txtTgl2.Value = Now

        '---load mesin aktif
        Dim xoItem As New clsMachineItem
        Dim xoMesin As New clsMachine
        
        With xoMesin
            .IsQueryMachine = False
            .IsShowAktifOnly = True
            .GetMachines()
        End With
        lbxMesin.Items.Clear()
        m_MachineList = xoMesin.Machines

        For Each xoItem In xoMesin.Machines
            lbxMesin.Items.Add("Mesin " & xoItem.MachineNumber & " (" & xoItem.IpAddress & ")")
        Next

        lbxLog.Items.Clear()

        '---set variables
        m_TransferSectionNum = -1
        m_QueryLogNum = -1
    End Sub

    Private Sub frmTransferManual_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        m_MachineList = Nothing
    End Sub

    Private Sub cmdProses_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub bwExec_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwExec.DoWork
        m_Transferer = New clsTransfer
        With m_Transferer
            .IsManualTransfer = True
            .StartDate = txtTgl1.Value
            .EndDate = txtTgl2.Value
        End With
        Dim i As Integer
        Dim xIdMesin As String
        For i = 0 To lbxMesin.Items.Count - 1
            If lbxMesin.GetItemChecked(i) Then
                xIdMesin = m_MachineList.GetItem(i).MachineNumber
                lbxLog.Items.Add("Mesin " & xIdMesin & " (" & m_MachineList.GetItem(i).IpAddress & ")")
                lbxLog.Items.Add("=============================")
                lbxLog.Items.Add("")
                m_Transferer.GetData(xIdMesin)
                m_Transferer.Transfer()
                lbxLog.Items.Add("")
            End If
        Next
        m_Transferer = Nothing

        'System.Threading.Thread.Sleep(5000)

    End Sub

    Private Sub bwExec_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwExec.RunWorkerCompleted
        cmdProses.Enabled = True
        Dim i As Integer
        For i = 0 To pnlFilter.Controls.Count - 1
            pnlFilter.Controls.Item(i).Enabled = True
        Next
        cmdClose.Enabled = True
    End Sub

    Private Sub cmdButton_Click(sender As Object, e As EventArgs) Handles cmdProses.Click
        Dim xTag As Integer
        xTag = DirectCast(sender, Button).Tag

        Select Case xTag
            Case 1
                lbxLog.Items.Clear()
                If lbxMesin.CheckedItems.Count < 1 Then
                    MsgBox("Belum ada mesin yang dipilih", MsgBoxStyle.Exclamation, "Error")
                    Exit Sub
                End If
                Dim i As Integer
                For i = 0 To pnlFilter.Controls.Count - 1
                    pnlFilter.Controls.Item(i).Enabled = False
                Next
                cmdProses.Enabled = False
                cmdClose.Enabled = False
                bwExec.RunWorkerAsync()
            Case 2
                Close()
        End Select
    End Sub

    Private Sub m_Transferer_GetStatus(ByRef xData As String, ByRef xSectionNum As Integer) Handles m_Transferer.GetStatus
        Select Case xSectionNum
            Case eStatusMesinEvent.estConnectMesin
                Dim xTmp As String
                Dim xChr As String : xChr = ""
                xTmp = Trim(lbxLog.Items.Item(lbxLog.Items.Count - 1))
                If xTmp <> "" Then xChr = ": "
                lbxLog.Items.Item(lbxLog.Items.Count - 1) = lbxLog.Items.Item(lbxLog.Items.Count - 1) & xChr & xData
            Case Else
                lbxLog.Items.Add(xData)
        End Select
    End Sub

    Private Sub m_Transferer_OnTransfer(ByRef xData As String, ByRef xSectionNum As Integer) Handles m_Transferer.OnTransfer
        If m_TransferSectionNum = xSectionNum Then
            lbxLog.Items.Item(lbxLog.Items.Count - 1) = "  " & xData
        Else
            lbxLog.Items.Add("- " & xData)
            lbxLog.Items.Add("")
            m_TransferSectionNum = xSectionNum
        End If
    End Sub

    Private Sub pnlFilter_Paint(sender As Object, e As PaintEventArgs) Handles pnlFilter.Paint

    End Sub

    Private Sub cmdClose_Click(sender As Object, e As EventArgs) Handles cmdClose.Click
        Me.Close()
    End Sub
End Class