﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransferManual
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdClose = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lbxLog = New System.Windows.Forms.ListBox()
        Me.bwExec = New System.ComponentModel.BackgroundWorker()
        Me.pnlFilter = New System.Windows.Forms.Panel()
        Me.cmdProses = New System.Windows.Forms.Button()
        Me.txtTgl2 = New System.Windows.Forms.DateTimePicker()
        Me.txtTgl1 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lbxMesin = New System.Windows.Forms.CheckedListBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.pnlFilter.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdClose
        '
        Me.cmdClose.Location = New System.Drawing.Point(568, 411)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(104, 27)
        Me.cmdClose.TabIndex = 9
        Me.cmdClose.Tag = "2"
        Me.cmdClose.Text = "Close"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(304, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Process Log:"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 444)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(679, 22)
        Me.StatusStrip1.TabIndex = 11
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lbxLog
        '
        Me.lbxLog.Font = New System.Drawing.Font("Lucida Console", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbxLog.FormattingEnabled = True
        Me.lbxLog.HorizontalScrollbar = True
        Me.lbxLog.ItemHeight = 12
        Me.lbxLog.Location = New System.Drawing.Point(8, 118)
        Me.lbxLog.Name = "lbxLog"
        Me.lbxLog.ScrollAlwaysVisible = True
        Me.lbxLog.Size = New System.Drawing.Size(664, 280)
        Me.lbxLog.TabIndex = 12
        '
        'bwExec
        '
        Me.bwExec.WorkerReportsProgress = True
        Me.bwExec.WorkerSupportsCancellation = True
        '
        'pnlFilter
        '
        Me.pnlFilter.Controls.Add(Me.cmdProses)
        Me.pnlFilter.Controls.Add(Me.txtTgl2)
        Me.pnlFilter.Controls.Add(Me.txtTgl1)
        Me.pnlFilter.Controls.Add(Me.Label2)
        Me.pnlFilter.Controls.Add(Me.Label1)
        Me.pnlFilter.Controls.Add(Me.lbxMesin)
        Me.pnlFilter.Controls.Add(Me.Label4)
        Me.pnlFilter.Location = New System.Drawing.Point(0, 0)
        Me.pnlFilter.Name = "pnlFilter"
        Me.pnlFilter.Size = New System.Drawing.Size(680, 112)
        Me.pnlFilter.TabIndex = 14
        '
        'cmdProses
        '
        Me.cmdProses.Location = New System.Drawing.Point(552, 72)
        Me.cmdProses.Name = "cmdProses"
        Me.cmdProses.Size = New System.Drawing.Size(120, 27)
        Me.cmdProses.TabIndex = 22
        Me.cmdProses.Tag = "1"
        Me.cmdProses.Text = "Proses"
        Me.cmdProses.UseVisualStyleBackColor = True
        '
        'txtTgl2
        '
        Me.txtTgl2.Cursor = System.Windows.Forms.Cursors.Default
        Me.txtTgl2.CustomFormat = "dd-MM-yyyy HH:mm:ss"
        Me.txtTgl2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtTgl2.Location = New System.Drawing.Point(464, 34)
        Me.txtTgl2.Name = "txtTgl2"
        Me.txtTgl2.Size = New System.Drawing.Size(204, 21)
        Me.txtTgl2.TabIndex = 21
        '
        'txtTgl1
        '
        Me.txtTgl1.CustomFormat = "dd-MM-yyyy HH:mm:ss"
        Me.txtTgl1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtTgl1.Location = New System.Drawing.Point(464, 8)
        Me.txtTgl1.Name = "txtTgl1"
        Me.txtTgl1.Size = New System.Drawing.Size(204, 21)
        Me.txtTgl1.TabIndex = 20
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(397, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Tgl. Akhir:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(396, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Tgl. Mulai:"
        '
        'lbxMesin
        '
        Me.lbxMesin.FormattingEnabled = True
        Me.lbxMesin.Location = New System.Drawing.Point(76, 8)
        Me.lbxMesin.Name = "lbxMesin"
        Me.lbxMesin.Size = New System.Drawing.Size(292, 100)
        Me.lbxMesin.Sorted = True
        Me.lbxMesin.TabIndex = 17
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 13)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Mesin Aktif:"
        '
        'frmTransferManual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(679, 466)
        Me.Controls.Add(Me.pnlFilter)
        Me.Controls.Add(Me.lbxLog)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cmdClose)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTransferManual"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Download Log Data"
        Me.pnlFilter.ResumeLayout(False)
        Me.pnlFilter.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lbxLog As System.Windows.Forms.ListBox
    Friend WithEvents bwExec As System.ComponentModel.BackgroundWorker
    Friend WithEvents pnlFilter As System.Windows.Forms.Panel
    Friend WithEvents lbxMesin As System.Windows.Forms.CheckedListBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmdProses As System.Windows.Forms.Button
    Friend WithEvents txtTgl2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtTgl1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
