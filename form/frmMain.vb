﻿Option Explicit On

Imports BiometricExport.modMain
Imports BiometricExport.modGridView

Public Class frmMain
    Private m_DbConnected As Boolean
    Private WithEvents m_Transferer As clsTransfer
    Private m_SelectedId As String

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '---GRID
        GridAddColumn(grdMesin, "IP Address", GridViewColType.gvcString, 130, True)
        GridAddColumn(grdMesin, "Keterangan", GridViewColType.gvcString, 150, True)
        GridAddColumn(grdMesin, "ID", GridViewColType.gvcNumber, 0, False)
        GridAddColumn(grdMesin, "Enabled", GridViewColType.gvcBool, 50, True)
        GridAddColumn(grdMesin, "IP", GridViewColType.gvcString, 0, False)
        GridAddColumn(grdMesin, "PORT", GridViewColType.gvcString, 0, False)

        LoadMachineList()

        m_Transferer = New clsTransfer

        CheckForIllegalCrossThreadCalls = False

        '--cek jika sudah disetting apa belum
        Dim xoAppConfig As New clsAppSetting
        If Trim(xoAppConfig.DbaMachineLogConfig.ServerName) = "" Then
ConfigServer:
            If frmSetupConn.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                tmrList.Enabled = True
                tmrDownload.Enabled = True
                btnRefresh.PerformClick()
            Else
                End
            End If
        Else
            Dim xoAdapter As New clsDbAdapter
            If Not xoAdapter.ConnectToServer(gConn, xoAppConfig.DbaMachineLogConfig) Then
                MsgBox("Unable connect to database server", MsgBoxStyle.Exclamation, "Error")
                GoTo ConfigServer
            Else
                tmrList.Enabled = True
                tmrDownload.Enabled = True
                btnRefresh.PerformClick()
            End If
            xoAdapter = Nothing
        End If

        meSetAsStartup()
    End Sub

    Private Sub frmMain_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        m_Transferer = Nothing
    End Sub

    Sub LoadMachineList(Optional ByVal xIndex As Integer = 0)
        On Error Resume Next

        Dim xMachines As New clsMachine
        Dim xItem As New clsMachineItem

        Dim xRowCount As Integer
        Dim i As Integer
        Dim xRow As DataGridViewRow
        Dim xKey As String
        xRowCount = grdMesin.Rows.Count
        If xRowCount > 0 Then
            For i = 0 To xRowCount - 1
                If grdMesin.Rows.Item(i).Selected Then
                    xKey = grdMesin.Rows.Item(i).Cells.Item(GridGetColIndexByHeader(grdMesin, "ID")).Value
                End If
            Next i
        End If

        xMachines.GetMachines()

        i = 0
        grdMesin.Rows.Clear()
        For Each xItem In xMachines.Machines
            grdMesin.Rows.Add(New String() {xItem.IpAddress & ":" & xItem.Port, xItem.Keterangan, xItem.MachineNumber, _
                                            xItem.IsEnabled, xItem.IpAddress, xItem.Port})

            If Trim(xKey) <> "" Then
                If xItem.MachineNumber = xKey Then
                    grdMesin.Rows.Item(i).Selected = True
                End If
            End If

            i = i + 1
        Next

        xMachines = Nothing
    End Sub

    Private Sub btnToolbarClick(sender As Object, e As EventArgs) Handles btnConfig.Click, ToolStripButton2.Click, btnDownload.Click, btnRefresh.Click
        Dim xTag As Integer

        xTag = DirectCast(sender, ToolStripButton).Tag
        Select Case xTag
            Case 1
                LoadMachineList()
            Case 2
                If frmLogin.ShowDialog(Me) Then
                    If frmLogin.FormResult Then
                        If frmTransferManual.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then

                        End If
                    End If
                End If
            Case 3
                Dim i As Integer
                For i = 0 To grdMesin.Rows.Count - 1
                    If grdMesin.Rows.Item(i).Selected Then
                        Exit For
                    End If
                Next i

                frmDeviceInfo.IdMesin = grdMesin.Rows.Item(i).Cells.Item(GridGetColIndexByHeader(grdMesin, "ID")).Value
                If frmDeviceInfo.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then

                End If
            Case 4
                If frmSetupConn.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then

                Else
                End If
        End Select
    End Sub

    Private Sub tmrList_Tick(sender As Object, e As EventArgs) Handles tmrList.Tick
        LoadMachineList()
    End Sub

    Private Sub MainMenuClick(sender As Object, e As EventArgs) Handles mnDownload.Click, mnSetupConn.Click
        Dim xTag As Integer
        xTag = DirectCast(sender, ToolStripMenuItem).Tag

        Select Case xTag
            Case 1
                btnConfig.PerformClick()
            Case 2
                btnDownload.PerformClick()
        End Select
    End Sub

    Private Sub DoTransfer()
        Dim xTransfer As New clsTransfer
        With xTransfer
            .IsManualTransfer = False
        End With

        Dim xMesin As New clsMachine
        Dim xMesinItem As New clsMachineItem

        With xMesin
            .IsQueryMachine = False
            .IsShowAktifOnly = True
            .GetMachines()
        End With

        For Each xMesinItem In xMesin.Machines
            m_Transferer.GetData(xMesinItem.MachineNumber)
            m_Transferer.Transfer()
        Next
    End Sub

    Private Sub tmrDownload_Tick(sender As Object, e As EventArgs) Handles tmrDownload.Tick
        '---download otomatis
        Dim xCfg As New clsAppSetting
        stsInfo1.Text = "Idle"
        btnDownload.Enabled = True
        'trigger
        'Dim xCurrentTime As DateTime : xCurrentTime = Now
        'If xCurrentTime >= m_Transferer.GetEndTimeConfig(xCfg.DbaMachineLogConfig.ExecTime) And xCurrentTime <= Now + New TimeSpan(23, 0, 0) Then
        '    DirectCast(sender, Timer).Enabled = False
        '    bwTransfer.RunWorkerAsync()
        'End If

        'realtime
        Dim xCurrentTime As DateTime : xCurrentTime = Now
        If xCurrentTime >= m_Transferer.GetEndTimeConfig(xCfg.DbaMachineLogConfig.ExecTime) Then
            btnDownload.Enabled = False
            DirectCast(sender, Timer).Enabled = False
            bwTransfer.RunWorkerAsync()
        End If

        xCfg = Nothing
    End Sub

    Private Sub frmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If MsgBox("Exit daemon ? ", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            e.Cancel = True
        End If

    End Sub

    Private Sub frmMain_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Select Case Me.WindowState
            Case FormWindowState.Minimized
                trayIcon.Visible = True
                Me.ShowInTaskbar = False
            Case FormWindowState.Normal
                trayIcon.Visible = False
                Me.ShowInTaskbar = True
        End Select
    End Sub

    Private Sub trayIcon_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles trayIcon.MouseDoubleClick
        Me.WindowState = FormWindowState.Normal
    End Sub

    Private Sub bwTransfer_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwTransfer.DoWork
        DoTransfer()
    End Sub

    Private Sub bwTransfer_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwTransfer.RunWorkerCompleted
        tmrDownload.Enabled = True
    End Sub

    Private Sub m_Transferer_GetStatus(ByRef xData As String, ByRef xSectionNum As Integer) Handles m_Transferer.GetStatus
        If Trim(xData) <> "" Then
            stsInfo1.Text = "Transferring in progress"
        Else
            stsInfo1.Text = "Idle"
        End If
    End Sub

    Private Sub m_Transferer_OnTransfer(ByRef xData As String, ByRef xSectionNum As Integer) Handles m_Transferer.OnTransfer
        If Trim(xData) <> "" Then
            stsInfo1.Text = "Transferring in progress"
        Else
            stsInfo1.Text = "Idle"
        End If
    End Sub
End Class
