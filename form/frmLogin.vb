﻿Public Class frmLogin
    Private m_Result As Boolean

    Property FormResult As Boolean
        Get
            Return m_Result
        End Get
        Set(value As Boolean)
            m_Result = value
        End Set
    End Property

    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        m_Result = False

        txtUser.Text = ""
        txtPass.Text = ""
    End Sub

    Private Sub cmdButtonClick(sender As Object, e As EventArgs) Handles cmdProses.Click, cmdCancel.Click
        Dim xTag As Integer : xTag = DirectCast(sender, Button).Tag
        Select Case xTag
            Case 1
                Dim xoUsr As New clsAksesUser
                With xoUsr
                    .UserName = txtUser.Text
                    If Not .CanLogin(txtPass.Text) Then
                        MsgBox(.ErrMsg, MsgBoxStyle.Exclamation, "Error")
                        m_Result = False
                    Else
                        m_Result = True
                        Close()
                    End If
                End With
                xoUsr = Nothing
            Case 2
                m_Result = False
                Close()
        End Select
    End Sub
End Class